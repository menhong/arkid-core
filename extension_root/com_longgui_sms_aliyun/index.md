# 短信插件：阿里云短信



## 抽象方法实现
* [load](#extension_root.com_longgui_sms_aliyun.AliyunSMSExtension.load)
* [send_sms](#extension_root.com_longgui_sms_aliyun.AliyunSMSExtension.send_sms)

## 代码

::: extension_root.com_longgui_sms_aliyun.AliyunSMSExtension
    rendering:
        show_source: true